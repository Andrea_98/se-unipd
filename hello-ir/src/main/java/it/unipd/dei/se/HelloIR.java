/*
 * Copyright 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.unipd.dei.se;


import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.simple.SimpleQueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Introductory example on how to use <a href="https://lucene.apache.org/" target="_blank">Apache Lucene</a> to index
 * and search for toy documents.
 *
 * @author Nicola Ferro (ferro@dei.unipd.it)
 * @version 1.0
 * @since 1.0
 */
public class HelloIR {

    /**
     * The first toy document.
     *
     * Credits to Maria Maistro.
     */
    private static final String D1 =
            "The quokka, the only member of the genus Setonix, is a small marsupial about the size of a domestic cat";

    /**
     * The second toy document.
     *
     * Credits to Maria Maistro.
     */
    private static final String D2 =
            "Wombats are small, short-legged, muscular quadrupedal marsupials that are native to Australia";

    /**
     * The third toy document.
     *
     * Credits to Maria Maistro.
     */
    private static final String D3 =
            "Quokkas have little fear of humans and commonly approach people closely, particularly on Rottnest Island in Australia, where a prevalent population exists";

    /**
     * The name of the {@code id} {@link org.apache.lucene.document.Field} in a {@link Document}.
     */
    private static final String ID = "id";

    /**
     * The name of the {@code body} {@link org.apache.lucene.document.Field} in a {@link Document}.
     */
    private static final String BODY = "body";

    /**
     * The default path to the directory containing the index, if none is provided.
     */
    private static final String DEFAULT_INDEX_PATH = "experiment/index";

    /**
     * The default run file, if none is provided.
     */
    private static final String DEFAULT_RUN_FILE = "experiment/run.txt";

    /**
     * The identifier of the run to be used in the run file.
     */
    private static final String RUN_ID = "hello-IR";

    /**
     * The maximum number of documents to be retrieved for each query
     */
    private static final int MAX_DOCS_RETRIEVED = 10;

    /**
     * The path to the directory containing the index.
     */
    private final Path indexPath;

    /**
     * The run to be written
     */
    private final PrintWriter run;

    /**
     * Creates a new instance for the specified index directory and run file.
     *
     * @param indexPath the path where to store the index.
     * @param runFile   the file where to write the run.
     * @throws IOException if something goes wrong accessing the index directory or the run file.
     */
    public HelloIR(String indexPath, String runFile) throws IOException {

        System.out.printf("%n%n############ HELLO, IR! ############%n%n");

        // check whether we have a path for the index or we should use the default one
        if (indexPath == null || indexPath.isEmpty()) {
            indexPath = DEFAULT_INDEX_PATH;
        }

        // check whether we have a path for the run file or we should use the default one
        if (runFile == null || runFile.isEmpty()) {
            runFile = DEFAULT_RUN_FILE;
        }

        // get access to the file system
        final FileSystem fs = FileSystems.getDefault();

        // get the path where to store the index
        this.indexPath = fs.getPath(indexPath);

        // if the directory does not already exist, create it
        if (Files.notExists(this.indexPath)) {
            Files.createDirectory(this.indexPath);
        }

        // check that the path is a directory (only needed if it already existed, always true if we created it)
        if (!Files.isDirectory(this.indexPath)) {
            throw new IllegalStateException(
                    String.format("%s is not a directory.", this.indexPath.toAbsolutePath().toString()));
        }

        final Path runPath = fs.getPath(runFile);

        // create a new run file, replacing the existing one, if any
        run = new PrintWriter(Files.newBufferedWriter(runPath, StandardCharsets.UTF_8, StandardOpenOption.CREATE,
                                                      StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE));

        System.out.printf("%n------------- INITIALIZING -------------%n");
        System.out.printf("- Index stored at: %s%n", this.indexPath.toAbsolutePath().toString());
        System.out.printf("- Run file at: %s%n", runPath.toAbsolutePath().toString());
        System.out.printf("----------------------------------------%n");
    }

    /**
     * Creates a new instance using the default path for the index directory and the default file for the run.
     *
     * @throws IOException if something goes wrong accessing the index directory or the run file.
     */
    public HelloIR() throws IOException {
        this(null, null);
    }

    /**
     * Parses the toy documents and returns a list of {@link Document}s.
     *
     * @return the list of documents.
     */
    public List<Document> parseDocuments() {

        System.out.printf("%n------------- PARSING DOCUMENTS -------------%n");

        // the list of documents
        final ArrayList<Document> docs = new ArrayList<>();

        Document d = null;

        // create a new document for D1
        d = new Document();
        d.add(new Field(ID, "d1", TextField.TYPE_STORED));
        d.add(new Field(BODY, D1, TextField.TYPE_STORED));
        docs.add(d);

        // create a new document for D2
        d = new Document();
        d.add(new Field(ID, "d2", TextField.TYPE_STORED));
        d.add(new Field(BODY, D2, TextField.TYPE_STORED));
        docs.add(d);

        // create a new document for D3
        d = new Document();
        d.add(new Field(ID, "d3", TextField.TYPE_STORED));
        d.add(new Field(BODY, D3, TextField.TYPE_STORED));
        docs.add(d);

        System.out.printf("The documents are:%n");
        docs.forEach(System.out::println);
        System.out.printf("---------------------------------------------%n");

        return docs;
    }

    /**
     * Indexes the provided {@link Document}s with the given {@link Analyzer}.
     *
     * @param docs     the documents to be indexed.
     * @param analyzer the analyzer to be used.
     * @throws IOException if something goes wrong while indexing.
     */
    public void index(final List<Document> docs, final Analyzer analyzer) throws IOException {

        System.out.printf("%n------------- INDEXING DOCUMENTS -------------%n");

        // Open the directory in Lucene
        final Directory directory = FSDirectory.open(indexPath);

        // Utility class for holding all the required configuration for the indexer
        final IndexWriterConfig config = new IndexWriterConfig(analyzer);

        // force to re-create the index if it already exists
        config.setOpenMode(IndexWriterConfig.OpenMode.CREATE);

        // set the similarity. BM25 is already the default one
        config.setSimilarity(new BM25Similarity());

        // The actual indexer
        final IndexWriter writer = new IndexWriter(directory, config);

        System.out.printf("- Indexer successfully created%n");

        for (Document d : docs) {
            writer.addDocument(d);

            System.out.printf("- Document %s successfully indexed%n", d.get(ID));
        }

        writer.close();
        directory.close();
        System.out.printf("- Indexer successfully closed%n");

        System.out.printf("----------------------------------------------%n");

    }

    /**
     * Searches for the provided query, by processing it with the given {@link Analyzer}.
     *
     * @param queryID  the identifier of the query.
     * @param query    the query to search for.
     * @param analyzer the analyzer to be used.
     * @throws IOException if something goes wrong while searching.
     */
    public void search(final String queryID, final String query, final Analyzer analyzer) throws IOException {

        System.out.printf("%n------------- SEARCHING DOCUMENTS FOR QUERY %s -------------%n", queryID);

        // Open the directory in Lucene
        final Directory directory = FSDirectory.open(indexPath);

        // Reads the index
        final DirectoryReader reader = DirectoryReader.open(directory);

        // Searches the index
        final IndexSearcher searcher = new IndexSearcher(reader);

        // set the similarity. BM25 is already the default one
        searcher.setSimilarity(new BM25Similarity());

        System.out.printf("- Searcher successfully created%n");

        // Parses the textual query
        final SimpleQueryParser qp = new SimpleQueryParser(analyzer, BODY);

        final Query q = qp.parse(query);

        System.out.printf("- Query successfully parsed: %s%n", q.toString());

        // Perform the actual search
        final ScoreDoc[] hits = searcher.search(q, MAX_DOCS_RETRIEVED).scoreDocs;

        System.out.printf("- %d documents retrieved%n", hits.length);

        // write the list to the run file and to the console
        for (int i = 0, n = hits.length; i < n; i++) {
            String docID = reader.document(hits[i].doc).get(ID);

            // write to the run file
            run.printf(Locale.ENGLISH, "%s\tQ0\t%s\t%d\t%.6f\t%s%n", queryID, docID, i, hits[i].score, RUN_ID);

            // write to the console
            System.out.printf(Locale.ENGLISH, "   %s\tQ0\t%s\t%d\t%.6f\t%s%n", queryID, docID, i, hits[i].score,
                              RUN_ID);
        }

        // ensure the run is flushed to disk
        run.flush();

        reader.close();
        directory.close();
        System.out.printf("- Searcher successfully closed%n");

        System.out.printf("-------------------------------------------------------------%n");
    }

    /**
     * Closes and releases resources.
     */
    public void close() {

        System.out.printf("%n------------- CLOSING -------------%n");

        run.close();

        System.out.printf("- Run file closed%n");
        System.out.printf("-----------------------------------%n");

        System.out.printf("%n%n############ BYE BYE, IR! ############%n%n");
    }


    /**
     * Main method of the class.
     *
     * @param args command line arguments. If provided, {@code args[0]} contains the path the the index directory;
     *             {@code args[1]} contains the path to the run file.
     * @throws IOException if something goes wrong while indexing and searching.
     */
    public static void main(String[] args) throws IOException {

        final HelloIR hir;

        if (args.length == 2) {
            hir = new HelloIR(args[0], args[1]);
        } else {
            hir = new HelloIR();
        }

        // create the list of documents from the provided toy ones
        final List<Document> docs = hir.parseDocuments();

        // The analyzer to be used for document pre-processing
        // The StandardAnalyzer tokenizes documents using spaces and recognizing, e.g., urls; then it turns tokens
        // lower-case and removes stop words.
        final Analyzer analyzer = new StandardAnalyzer();

        // index the documents
        hir.index(docs, analyzer);

        // search the documents
        hir.search("001", "quokka", analyzer);
        hir.search("002", "Australia animals", analyzer);
        hir.search("003", "small marsupial", analyzer);
        hir.search("004", "quokka australia", analyzer);

        // release resources
        hir.close();

    }

}
