%
% Copyright 2021-2022 University of Padua, Italy
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
% Author: Nicola Ferro (ferro@dei.unipd.it)


% A topic of a run. 
% Each position is a retrieved document; first element of the vector is 
% the top of the rank. 
% Assume binary relevance: 0 for not relevant; 1 for relevant.
run = [0 1 1 0 1 0 0 1 1 0];

% Recall Base
RB = 5;

% Recall at each rank position
recall = cumsum(run) ./ RB;

% Precision at each rank position
precision = cumsum(run)./ (1:length(run));

% Rprec
rprec = NaN;
if (RB <= length(run))
    rprec = precision(RB);
end

% Standard Recall levels
recallLevel = 0:0.1:1;

% 11-points iterpolated precision 
iPrecision = zeros(1, length(recallLevel));

for i = 1:length(recallLevel)
    
    % find the recall values R that are greater than or equal to the
    % current standard recall level R_i
    idx = recall >= recallLevel(i);
    
    % if there is any such recall value, take the maximum precision
    % otherwise interpolated precision is zero at that standard recall
    % level, i.e. skip any assignment
    if (any(idx))
        iPrecision(i) = max(precision(idx));
    end    
end
