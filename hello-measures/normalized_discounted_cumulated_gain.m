%
% Copyright 2021-2022 University of Padua, Italy
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
% Author: Nicola Ferro (ferro@dei.unipd.it)


% A topic of a run. 
% Each position is a retrieved document; first element of the vector is 
% the top of the rank. 
% Assume graded relevance: 0 for not relevant; 1 for partially relevant; 
% 2 for fairly relevant; 3 for highly relevant
run =   [3 0 1 2 0 0 0 2 0 0];

% the ideal run
ideal = [3 3 2 2 2 1 1 1 0 0];

% the base of the logarithm
b = 2;

% DCG at each rank position
if (b == 2)     % impatient user
    dcg = cumsum(run ./ max(1, log2(1:length(run))));
    dcg_ideal = cumsum(ideal ./ max(1, log2(1:length(ideal))));
else            % patient user
    dcg = cumsum(run ./ max(1, log10(1:length(run))));
    dcg_ideal = cumsum(ideal ./ max(1, log10(1:length(ideal))));
end

ndcg = dcg ./ dcg_ideal;
